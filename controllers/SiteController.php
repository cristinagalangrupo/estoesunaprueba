<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Fotos;
use app\models\Paginas;

class SiteController extends Controller
{
    

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $datos=Fotos::find()
                ->where(['portada'=>1])
                ->all();
        return $this->render('index',[
            "datos"=>$datos,
        ]);
    }
     public function actionFotos()
    {
        $datos=Fotos::find()
                ->where(['portada'=>0])
                ->all();
        return $this->render('fotos',[
            "datos"=>$datos,
        ]);
    }
    public function actionDonde(){
        $pagina = Paginas::find()
            ->where(['nombre' => 'Dónde estamos'])
            ->one();
        
        return $this->render('dondeEstamos',[
            "pagina"=>$pagina
        ]);
    }
    public  function actionPagina($p){
        $pagina = Paginas::find()
            ->where(['nombre' => 'Página '.$p])
            ->one();
        return $this->render('paginas',[
            'pagina'=>$pagina,
        ]);
    }
    
}
